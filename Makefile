app_name = auditssl
export SERVICE_NAME=$(app_name)
.PHONY: build tag push
build:
	@build-image.sh
tag:
	@docker tag $(app_name) nexus.ad.cfcc.edu:18095/$(app_name)
push: build tag
	@docker push nexus.ad.cfcc.edu:18095/$(app_name)
