"""Test the database by creating some host records.

To use, create a CSV file called 'hosts.csv' in the following format:

hostname-one.example.com,192.168.0.1,443
hostname-two.example.com,192.168.0.2,56789

"""

from auditssl import create_app, model
import csv

app = create_app()

with app.app_context():
    model.connect_db()
    with open('hosts.csv', newline='') as fn:
        my_reader = csv.reader(fn)
        for row in my_reader:
            print("[DEBUG] adding", repr(row))
            try:
                port_number = int(row[2])
                my_host = model.Host(name=row[0], port=port_number)
                my_host.ipAddress = row[1]
            except ValueError as ex:
                print("Error adding host:", ex)
