#!/usr/bin/env python3
""" This script scans an IP range for active hosts and then checks to see if they have web server with SSL.

There is a cron task for 'jfriant' on zabbix.ad.cfcc.edu that runs from a virtual environment:

    30 */2 * * * $HOME/opt/auditssl/venv/bin/python3 $HOME/opt/auditssl/scan_ips.py -f -q
"""
import argparse
import datetime
import os
import socket
import struct
import ssl
import OpenSSL.crypto as crypto
import sys

from rich.console import Console
from rich.progress import Progress
from dateutil import parser
from sqlobject import SQLObjectNotFound
from sqlobject.dberrors import DuplicateEntryError

from ping3 import ping

from auditssl import create_app
from auditssl import model

USE_PING3 = True
PING_TIMED_OUT = -1
PING_UNKNOWN = -2
DEFAULT_TIMEOUT = 5
DEFAULT_PORT = 443

console = Console()


def ip2int(addr):
    return struct.unpack("!I", socket.inet_aton(addr))[0]


def int2ip(addr):
    return socket.inet_ntoa(struct.pack("!I", addr))


def print_progress(iteration, total, prefix='', suffix='', decimals=1, bar_length=100):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        bar_length  - Optional  : character length of bar (Int)

    From: https://gist.github.com/aubricus/f91fb55dc6ba5557fbab06119420dd6a
    """
    # percents = f'{100 * (iteration / float(total)):.2f}'
    str_format = "{0:." + str(decimals) + "f}"
    percents = str_format.format(100 * (iteration / float(total)))
    filled_length = int(round(bar_length * iteration / float(total)))
    # bar = f'{"█" * filled_length}{"-" * (bar_length - filled_length)}'
    bar = '{}{}'.format("█" * filled_length,
                        "-" * (bar_length - filled_length))

    # sys.stdout.write(f'\r{prefix} |{bar}| {percents}% {suffix}')
    sys.stdout.write('\r{prefix} |{bar}| {percents}% {suffix}'.format(prefix=prefix,
                                                                      bar=bar,
                                                                      percents=percents,
                                                                      suffix=suffix))
    # '\r%s |%s| %s%s %s' % (prefix, bar, percents, '%', suffix)

    if iteration == total:
        sys.stdout.write('\n')
    sys.stdout.flush()


def get_cert(hostname, addr, port=DEFAULT_PORT, timeout=None, debug=False):
    """Open a socket connection to the host and return the SSL Certificate information.

    A database entry for the SSL certificates is created if it doesn't already exist.

    """
    context = ssl.create_default_context()
    context.check_hostname = False
    context.verify_mode = ssl.CERT_NONE
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        sock.settimeout(timeout)
        if port in (25, 587):
            sock.connect((addr, port))
            sock.recv(1000)
            sock.send(b'EHLO\nSTARTTLS\n')
            sock.recv(1000)
            ssock = context.wrap_socket(sock, server_hostname=hostname)
        else:
            ssock = context.wrap_socket(sock, server_hostname=hostname)
            ssock.connect((addr, port))
        cert_bin = ssock.getpeercert(True)
        x509 = crypto.load_certificate(crypto.FILETYPE_ASN1, cert_bin)

        cert_serial = model.convert_serial_number(x509.get_serial_number())
        if debug:
            print('[DEBUG]', hostname, cert_serial)
    try:
        cert_rec = model.Certificate.bySerialNumber(cert_serial)
    except SQLObjectNotFound:
        cert_rec = None

    if cert_rec is None:
        try:
            cert_rec = model.Certificate(
                serialNumber=cert_serial,
                commonName=x509.get_subject().commonName,
                issuerName=x509.get_issuer().organizationName,
                notBefore=parser.parse(x509.get_notBefore()),
                notAfter=parser.parse(x509.get_notAfter()),
            )
        except DuplicateEntryError:
            pass

    return cert_rec


def run_scan(host, quiet=True, debug=False):
    """Ping and connect to the host if it responds, returns True if a certificate is found."""
    cert_found = False
    try:
        if host.ipAddrNum == 0:
            # hosts without an IP address are CNAME's in DNS
            ip_addr = socket.gethostbyname(host.name)
            if not quiet:
                console.print('[INFO] Found IP address {1} for {0}'.format(host.name, ip_addr))
        else:
            ip_addr = host.ipAddress
        if not host.noPing:
            if debug:
                console.print("[DEBUG] pinging {}".format(ip_addr))
            if USE_PING3:
                ping_result = ping(ip_addr)
                if debug:
                    console.print("[DEBUG] ping3 result was: {}".format(ping_result))
                if ping_result is None:
                    response = PING_TIMED_OUT
                elif not ping_result:
                    response = PING_UNKNOWN
                else:
                    response = 0
            else:
                if os.name == 'nt':
                    response = os.system("ping.exe -n 1 {} > NUL".format(ip_addr))
                    if debug:
                        console.print("[DEBUG] ping.exe result was: {}".format(response))
                else:
                    response = os.system("ping -q -c 1 {} >/dev/null".format(ip_addr))
                    if debug:
                        console.print("[DEBUG] ping result was: {}".format(response))
        else:
            response = 0
        if response == 0:
            cert_rec = get_cert(host.name, ip_addr, host.port, DEFAULT_TIMEOUT)
            host.certificate = cert_rec
            host.errorMessage = ""
            cert_found = True
            if debug:
                console.print("[DEBUG] updated certificate")
        else:
            host.errorMessage = "Ping error: host did not respond"
            # removing the certificate from this host record if the host no longer responds
            if host.certificate is not None:
                host.certificate = None
                if debug:
                    print("[DEBUG] removed certificate from unresponsive host")
    except (ConnectionRefusedError, ssl.SSLError) as exc:
        host.errorMessage = str(exc)
        if host.certificate is not None:
            host.certificate = None
            if debug:
                console.print("[DEBUG] Connection error, removed certificate")
    except (socket.timeout, ConnectionResetError, OSError) as exc:
        host.errorMessage = 'Connection Error: ' + str(exc)
        if host.certificate is not None:
            host.certificate = None
            if debug:
                console.print("[DEBUG] Connection error, removed certificate")
    host.lastChecked = datetime.datetime.now()
    return cert_found


def update_hosts(with_certificates=True, quiet=False):
    if with_certificates:
        host_list = model.Host.select(model.Host.q.certificate != 0)
    else:
        host_list = model.Host.select()
    host_cnt = len(list(host_list))
    if quiet:
        visible = False
    else:
        visible = True
    with Progress(console=console) as progress:
        task1 = progress.add_task("Updating hosts...", total=host_cnt, visible=visible)
        if not quiet:
            console.print("Updating {} host records".format(host_cnt))
        j = 0
        for host in host_list:
            if not quiet:
                j += 1
                progress.update(task1, advance=1.0)
                # print_progress(j, host_cnt, bar_length=60)
            run_scan(host, quiet)
        if not quiet:
            console.print()


def find_hosts(start_ip_address, progress, bitmask=24, quiet=False):
    if quiet:
        bar_visible = False
    else:
        bar_visible = True
    start_time = datetime.datetime.now()
    if not quiet:
        console.print("Scanning IP range {}/{}".format(start_ip_address, bitmask))
    start_ip = ip2int(start_ip_address)
    host_range = (2 ** (32 - bitmask)) - 1
    scanned_ip_cnt = 0
    certs_found_cnt = 0
    scan_task = progress.add_task(f"[green] {start_ip_address}/{bitmask}...", total=host_range, visible=bar_visible)
    for i in range(start_ip + 1, start_ip + host_range):
        if not quiet:
            scanned_ip_cnt += 1
            progress.update(scan_task, advance=1.0)
            # print_progress(scanned_ip_cnt, host_range, bar_length=60)

        try:
            ip_string = int2ip(i)
            s = model.Host.select(model.Host.q.ipAddrNum == i)
            if len(list(s)) > 0:
                host_rec = s[0]
            else:
                (name, alias_list, address_list) = socket.gethostbyaddr(ip_string)
                host_rec = model.Host(name=name, port=443)
                host_rec.ipAddress = ip_string

            success = run_scan(host_rec, quiet)
            if success:
                certs_found_cnt += 1
        except socket.herror:
            pass
    if not quiet:
        end_time = datetime.datetime.now()
        elapsed_time = end_time - start_time
        console.print("Found {} certificates out of {} IPs scanned.".format(certs_found_cnt, scanned_ip_cnt))
        console.print("Time to scan:", elapsed_time)
        console.print()


def main():

    arg_p = argparse.ArgumentParser()

    arg_p.add_argument("-a", "--update-all", dest="with_certificates",
                       action="store_false", default=True,
                       help="Scan all hosts in the database, not just ones with certs.")
    arg_p.add_argument("-d", "--debug",
                       action="store_true", default=False,
                       help="Print additional debugging messages")
    arg_p.add_argument("-f", "--find", dest="find_hosts",
                       action="store_true", default=False,
                       help="Scan IP address ranges to find new hosts")
    arg_p.add_argument("-q", "--quiet",
                       action="store_true", default=False,
                       help="Suppress all the extra status messages")
    arg_p.add_argument("--host-id", dest="host_id", default=None,
                       help="Select a host by ID to scan individually")

    args = arg_p.parse_args()

    app = create_app()
    with app.app_context():
        model.connect_db()
        my_networks = app.config['SUBNETS']

    if args.host_id is not None:
        this_host = model.Host.get(args.host_id)
        run_scan(this_host, args.quiet, args.debug)
    else:
        if args.find_hosts:
            with Progress(console=console) as progress:
                if args.quiet:
                    visible = False
                else:
                    visible = True
                net_task = progress.add_task("Scanning for hosts...", total=len(my_networks), visible=visible)
                for subnet, bitmask in my_networks:
                    progress.update(net_task, advance=1.0)
                    find_hosts(subnet, progress, bitmask=bitmask, quiet=args.quiet)
        else:
            update_hosts(with_certificates=args.with_certificates, quiet=args.quiet)


if __name__ == '__main__':
    main()
