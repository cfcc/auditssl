auditSSL
======
**auditSSL** is an application for scanning and reporting on the status of SSL certificates on local servers.

## Description

The application is in three parts:

1. A MySQL database
2. A front-end website written with Python 3 and Flask that runs in a Docker container
3. A back-end scanner written with Python 3

## Docker Build

The project has a docker-compose.yml file, so you can build and run the application during development.  For example:

```bash
docker-compose build
docker-compose up -d
docker-compose stop
```

To upload to our local Docker repository use the target in the Makefile.  For example:

```bash
make push
```

## Configuration

The Flask application has default configuration settings that are good for development, but when you run in production
you must create a ```instance/config.py``` file.

*  Contents need to be (at a minimum):

```python
SECRET_KEY = 'KEY_HERE'
DATABASE = 'mysql://username:password@hostname/dbname'
DB_DRIVER = 'pymysql'
```

*  The SUBNETS configuration variable is needed by the scanner and let you specify as many networks as you would like to
   be scanned.

```python
SUBNETS = (('192.168.0.0', 24),
           ('10.10.0.0', 16),
           ('172.18.1.0', 24))
```

## Installation

Once the Docker container has been tagged and pushed to the repository it can be installed as a normal container.  We
use docker-compose to configure and launch the containers.

Make sure to map a volume for the configuration file.  For example:

```yaml
 volumes:
    - $DOCKERDIR/auditssl:/app/instance/:ro
```
