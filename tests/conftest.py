import os
import tempfile

import pytest
from auditssl import create_app
from auditssl.model import connect_db, init_db


@pytest.fixture
def app():
    db_fd, db_path = tempfile.mkstemp()

    app = create_app({
        'TESTING': True,
        'DATABASE': 'sqlite:' + db_path,
    })

    with app.app_context():
        init_db()
        connect_db()

    yield app

    os.close(db_fd)
    os.unlink(db_path)


@pytest.fixture
def client(app):
    return app.test_client()


@pytest.fixture
def runner(app):
    return app.test_cli_runner()
