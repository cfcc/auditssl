FROM python:3.10-slim-bookworm

ARG GIT_HASH
ENV GIT_HASH=$(GIT_HASH:-dev)

# RUN apt-get update && apt-get install -y gcc libldap2-dev libsasl2-dev libssl-dev python3-dev musl-dev linux-musl-dev

WORKDIR /app

COPY requirements.txt .

RUN pip3 install --upgrade pip && pip3 install wheel
RUN pip3 install --no-cache-dir -r requirements.txt

RUN ["mkdir", "instance"]

COPY wsgi.py .
COPY conf/ ./conf/
COPY auditssl/ ./auditssl/

EXPOSE 8000

CMD ["gunicorn", "--config", "conf/gunicorn_config.py", "wsgi"]
