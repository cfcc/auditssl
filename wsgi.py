"""This file is used by Gunicorn and Docker to launch the Flask app."""
from auditssl import create_app

application = create_app()
