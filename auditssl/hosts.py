import sqlobject
from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)
from werkzeug.exceptions import abort

from auditssl.model import connect_db, Host

DEFAULT_PORT = 443

bp = Blueprint('hosts', __name__, url_prefix='/hosts')


@bp.route('/')
def index():
    connect_db()
    host_list = Host.select(orderBy=Host.q.ipAddrNum)
    return render_template('hosts/index.html', host_list=host_list)


@bp.route('/create', methods=('GET', 'POST'))
def create():
    connect_db()
    if request.method == 'POST':
        hostname = request.form['hostname']
        error = None

        if not hostname:
            error = 'A Host Name is required'

        if request.form['ip_addr']:
            ip_addr = request.form['ip_addr']
        else:
            ip_addr = "0"

        if request.form['port']:
            try:
                this_port = int(request.form['port'])
            except ValueError:
                this_port = DEFAULT_PORT
        else:
            this_port = DEFAULT_PORT

        if error is not None:
            flash(error)
        else:
            h = Host(name=hostname, port=this_port, certificate=None)
            h.ipAddress = ip_addr
            flash('Host created', 'success')
            return redirect(url_for('hosts.index'))

    return render_template('hosts/create.html')


@bp.route('/<host_id>/update', methods=('GET', 'POST'))
def update(host_id):
    connect_db()
    try:
        host = Host.get(host_id)
    except sqlobject.SQLObjectNotFound:
        host = None
        abort(404, "Host id {0} doesn't exist.".format(host_id))

    if request.method == 'POST':
        hostname = request.form['hostname']
        try:
            port = int(request.form['port'])
        except ValueError:
            port = DEFAULT_PORT
        if 'no_ping' in request.form:
            no_ping = True
        else:
            no_ping = False

        error = None
        if not hostname:
            error = 'A Host Name is required'

        if request.form['ip_addr']:
            ip_addr = request.form['ip_addr']
        else:
            ip_addr = "0"

        if error is not None:
            flash(error)
        else:
            host.name = hostname
            host.ipAddress = ip_addr
            host.port = port
            host.noPing = no_ping
            flash('Host updated successfully', 'success')
            return redirect(url_for('hosts.index'))

    return render_template('hosts/update.html', host=host)


@bp.route('/<host_id>/delete', methods=('POST',))
def delete(host_id):
    connect_db()
    Host.delete(host_id)
    flash('Host id {} deleted.'.format(host_id))
    return redirect(url_for('hosts.index'))
