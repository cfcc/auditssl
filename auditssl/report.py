import datetime
from sqlobject import AND

from flask import (
    Blueprint, render_template,
    jsonify, request)
from sqlobject.dberrors import OperationalError

from auditssl.model import connect_db, Host, Certificate

bp = Blueprint('report', __name__, url_prefix='/report')


@bp.route('/show')
def show_all():
    connect_db()
    hosts = Host.select(orderBy=Host.q.name)

    return render_template('report/index.html', hosts=hosts)


@bp.route('/certs')
def certs():
    connect_db()
    days = 0
    if 'expire' in request.args:
        if 'limit' in request.args:
            # Do not show any certificates that are more than 30 days expired
            limit_expired = datetime.datetime.now() - datetime.timedelta(days=30)
        else:
            # ...otherwise push the limit out to two years
            limit_expired = datetime.datetime.now() - datetime.timedelta(days=730)
        try:
            days = int(request.args['expire'])
            as_of = datetime.datetime.now() + datetime.timedelta(days=days)
        except ValueError:
            as_of = datetime.datetime.now()
        hosts = Host.select(AND(Host.q.certificate,
                                Certificate.q.notAfter >= limit_expired,
                                Certificate.q.notAfter <= as_of,
                                Host.q.certificateID == Certificate.q.id),
                            orderBy=Host.q.name)
    else:
        hosts = Host.select(Host.q.certificate, orderBy=Host.q.name)
    return render_template('report/index.html', hosts=hosts, days=days)


@bp.route('/stats/certs')
def cert_stats():
    try:
        connect_db()
        result1 = Host.select(Host.q.certificate != 0)
        host_with_cert = result1.count()

        result3 = [h for h in result1 if h.certificate.days_to_expire() <= 90]
        host_with_expire = len(result3)

        host_with_good_cert = host_with_cert - host_with_expire

        stats_out = {
            'cols': [
                {'label': 'Category', 'type': 'string'},
                {'label': 'Count', 'type': 'number'},
            ],
            'rows': [
                {'c': [{'v': 'Good Certificate'}, {'v': host_with_good_cert}]},
                {'c': [{'v': 'Expiring Certificate'}, {'v': host_with_expire}]},
            ],
        }
    except OperationalError:
        stats_out = {}
    return jsonify(stats_out)


@bp.route('/stats/hosts')
def host_stats():
    try:
        connect_db()
        result1 = Host.select(Host.q.certificate != 0)
        host_with_cert = result1.count()

        result2 = Host.select()
        total_hosts = result2.count()

        host_with_none = total_hosts - host_with_cert

        stats_out = {
            'cols': [
                {'label': 'Category', 'type': 'string'},
                {'label': 'Has Cert', 'type': 'number'},
                {'label': 'No Cert', 'type': 'number'},
            ],
            'rows': [
                {'c': [{'v': 'Hosts'}, {'v': host_with_cert}, {'v': host_with_none}]},
            ],
        }
    except OperationalError:
        stats_out = {}
    return jsonify(stats_out)
