from flask import (
    Blueprint, render_template,
    abort)
from sqlobject import SQLObjectNotFound

from auditssl.model import connect_db, Certificate

bp = Blueprint('certificates', __name__, url_prefix='/certificates')


@bp.route('/')
def index():
    connect_db()
    certs = Certificate.select(orderBy=Certificate.q.commonName)

    return render_template('certificates/index.html', certs=certs)


@bp.route('/<int:cert_id>')
def show(cert_id):
    connect_db()
    try:
        this_cert = Certificate.get(cert_id)
        return render_template('certificates/show.html', this_cert=this_cert)
    except SQLObjectNotFound:
        abort(404)
