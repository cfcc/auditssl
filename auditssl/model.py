import datetime
import socket
import struct

import sqlobject as db
from flask import current_app
from flask.cli import with_appcontext
import click


def connect_db(verbose=False):
    if 'DB_DRIVER' in current_app.config:
        connection = db.connectionForURI(current_app.config['DATABASE'], driver=current_app.config['DB_DRIVER'])
    else:
        connection = db.connectionForURI(current_app.config['DATABASE'])
    if verbose:
        print("Connecting to database:", connection)
    db.sqlhub.processConnection = connection


class Host(db.SQLObject):
    name = db.StringCol()
    ipAddrNum = db.BigIntCol(default=0)
    port = db.IntCol(default=443)
    noPing = db.BoolCol(default=False)

    errorMessage = db.StringCol(default="")
    lastChecked = db.DateTimeCol(default=None)

    certificate = db.ForeignKey('Certificate', default=None)

    def __str__(self):
        return "{} ({}:{})".format(self.name, self.ipAddress, self.port)

    def _get_ipAddress(self):
        if self.ipAddrNum > 0:
            return socket.inet_ntoa(struct.pack("!I", self.ipAddrNum))
        else:
            return ''

    def _set_ipAddress(self, addr):
        self.ipAddrNum = struct.unpack("!I", socket.inet_aton(addr))[0]

    def get_last_checked(self):
        """Convert the timestamp to a string"""
        if self.lastChecked is not None:
            return self.lastChecked.strftime("%b %d, %Y at %H:%M:%S")
        else:
            return "Never"

    def certificate_status(self):
        """Return a unicode emoji if the host has a current certificate.

        U+2705 = check mark button
        U+2604 = no entry
        U+2B50 = star
        U+1F4A3 = bomb
        """
        emoji_check = chr(0x2705)
        emoji_no_entry = chr(0x26D4)
        # emoji_star = chr(0x2b50)
        # emoji_bomb = chr(0x1f4a3)
        if self.certificate is not None:
            return emoji_check
        else:
            return emoji_no_entry


def convert_serial_number(sn):
    return '{:02X}'.format(sn)


class Certificate(db.SQLObject):
    serialNumber = db.StringCol(length=64, alternateID=True)
    commonName = db.StringCol(default="")
    issuerName = db.StringCol(default="")
    notBefore = db.DateTimeCol(default=None)
    notAfter = db.DateTimeCol(default=None)

    hosts = db.MultipleJoin('Host')

    def days_to_expire(self):
        if self.notAfter is not None:
            today = datetime.datetime.now()
            dt = self.notAfter - today
            return dt.days
        else:
            return 0

    def color_code(self):
        """Return a pair of CSS classes based on the number of days until expiration"""
        dte = self.days_to_expire()
        if 60 <= dte < 90:
            my_color = 'bg-info text-white'
        elif 30 <= dte < 60:
            my_color = 'bg-warning text-dark'
        elif 0 <= dte < 30:
            my_color = 'bg-danger text-white'
        elif dte < 0:
            my_color = 'bg-dark text-white'
        else:
            my_color = 'bg-success text-white'
        return my_color

    def __str__(self):
        return "{} - {} - {}".format(self.commonName, self.issuerName, self.days_to_expire())


def init_db():
    connect_db(verbose=True)
    Host.dropTable(ifExists=True)
    Certificate.dropTable(ifExists=True)
    Certificate.createTable()
    Host.createTable()


@click.command('init-db')
@with_appcontext
def init_db_command():
    init_db()
    click.echo("Initialized the database")


def init_app(app):
    app.cli.add_command(init_db_command)
