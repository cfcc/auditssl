import os

from flask import Flask, render_template


def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='dev',
        DATABASE="sqlite:" + os.path.join(app.instance_path, 'auditssl.sqlite'),
        SUBNETS=[]
    )

    if test_config is None:
        app.config.from_pyfile('config.py', silent=True)
    else:
        app.config.from_mapping(test_config)

    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    from . import model
    model.init_app(app)

    @app.route('/')
    def index():
        return render_template('index.html', my_networks=app.config['SUBNETS'])

    from . import report
    app.register_blueprint(report.bp)

    from . import hosts
    app.register_blueprint(hosts.bp)

    from . import certificates
    app.register_blueprint(certificates.bp)

    return app
