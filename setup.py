from setuptools import find_packages, setup

setup(
    name='auditssl',
    version='1.0.4',
    description='Scan and track SSL certificates on servers in the LAN',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'flask', 'click', 'sqlobject', 'rich', 'ping3'
    ],
)
