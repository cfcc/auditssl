#!/usr/bin/env python3
"""This file is used for Apache2 and Mod WSGI"""
activate_this = '/var/www/auditssl/venv/bin/activate_this.py'
with open(activate_this) as file_:
    exec(file_.read(), dict(__file__=activate_this))

from auditssl import create_app

application = create_app()
